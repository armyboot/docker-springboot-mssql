package com.tts.dockermssql.web;

import com.tts.dockermssql.service.UserService;
import com.tts.dockermssql.vo.UserAccess;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
@Slf4j
public class UserController {
    @Autowired
    UserService userService;

    @GetMapping("/")
    public String showUser() {
        return "redirect:/getUserList";
    }

    @GetMapping("/newUser")
    public String newUser() {
        return "addUser";
    }

    @GetMapping("/delete")
    public String delete(Long id) {
        userService.delete(id);
        return "redirect:/getUserList";
    }

    @PostMapping("/addUser")
    public String addUser(Model model, UserAccess user) {
        UserAccess result = userService.add(user);
        if(null != result) {
            model.addAttribute("user", result);
        }else {
            model.addAttribute("user", null);
        }

        return "redirect:/getUserList";
    }

    @GetMapping("/getUserById")
    public String getUserById(Model model, String userId) {
        log.info("getUserById " + userId);
        UserAccess user = userService.getUserById(userId);

        model.addAttribute("user", user);

        return "addUser";

    }

    @GetMapping("/getUserList")
    public String getUserList(Model model) {
        log.info("getUserList");
        List<UserAccess> userList = userService.getUsers();

        model.addAttribute("userList", userList);

        return "userList";

    }


}
