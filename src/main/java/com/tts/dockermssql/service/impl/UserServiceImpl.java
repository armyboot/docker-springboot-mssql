package com.tts.dockermssql.service.impl;

import com.tts.dockermssql.dao.UserDAO;
import com.tts.dockermssql.service.UserService;
import com.tts.dockermssql.vo.UserAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserDAO userDAO;

    @Override
    public UserAccess add(UserAccess user) {
        return userDAO.save(user);
    }

    @Override
    public boolean delete(Long id) {
        try {
            userDAO.deleteById(id);
            return true;
        }catch (Exception e) {
            return false;
        }
    }

    @Override
    public UserAccess getUserById(String userId) {
        return userDAO.findByUserId(userId);
    }

    @Override
    public List<UserAccess> getUsers() {
        return userDAO.findAll();
    }
}
