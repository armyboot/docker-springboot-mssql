package com.tts.dockermssql.service;

import com.tts.dockermssql.vo.UserAccess;

import java.util.List;

public interface UserService {

    UserAccess add(UserAccess user);

    boolean delete(Long id);

    UserAccess getUserById(String userId);

    List<UserAccess> getUsers();
}
