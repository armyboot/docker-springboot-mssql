package com.tts.dockermssql.vo;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
public class UserAccess implements Serializable {

    private static final long serialVersionUID = 3903863761981632394L;
    @Id
    @Column(nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String userId;
    private String userName;
    private String address;

}
