package com.tts.dockermssql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DockerMssqlApplication {

    public static void main(String[] args) {
        SpringApplication.run(DockerMssqlApplication.class, args);
    }

}
