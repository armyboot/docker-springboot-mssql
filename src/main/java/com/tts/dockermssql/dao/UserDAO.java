package com.tts.dockermssql.dao;

import com.tts.dockermssql.vo.UserAccess;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDAO extends JpaRepository<UserAccess, Long> {

    UserAccess findByUserId(String userId);
}
